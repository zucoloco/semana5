#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 14:03:24 2020

@author: gustavo
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: gustavo
Water flows between two parallel plates with a uniform
velocity of 0.02 m/s, the gap between the plates is 2.0 cm, 
and the length of each plate is 50 cm. Determine the velocity
profile and developing length of the flow. 
"""

import numpy as np
import matplotlib.pyplot as plt

n = 1000 #1000 lattices na direção x
m = 40 #40 lattices na direção y

rho = np.zeros((n, m)) 
u = np.zeros((n, m)) 
v = np.zeros((n, m))

feq = np.zeros((n, m, 9))
f = np.zeros((n, m, 9))
f_linha = np.zeros((n, m, 9)) #para atualizar o vetor na propagação sem sobrepor valores

w = np.array([4/9, 1/9, 1/9, 1/9, 1/9, 1/36, 1/36, 1/36, 1/36])
cx = np.array([0, 1, 0, -1, 0, 1, -1, -1, 1])
cy = np.array([0, 0, 1, 0, -1, 1, 1, -1, -1])



alpha = 0.02 #viscosidade cinemática
omega = (3 * alpha + 0.5)
Re = 400 #para manter a semelhança, devemos manter o reynolds inalterado, mesmo que alteremos os parâmetros

# u/u_in = 1.5 * (1 - y/H)

u_lattice = 0.2

tmpx = np.zeros(9)
tmpy = np.zeros(9)

for y in range(0, m): 
    for x in range(0, n):
        u[0, y] = u_lattice
        rho[x, y] = 1.0

        
for t in range(400):
    for x in range(0, n):
        for y in range(0, m):             
            feq[x, y, 0] = rho[x, y] * w[0] * (1 - 1.5 * (u[x, y] * u[x, y] + v[x,y] * v[x,y]))
            feq[x, y, 1] = rho[x, y] * w[1] * (1 + 3 * u[x, y] + 4.5 * u[x, y] * u[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y])) 
            feq[x, y, 2] = rho[x, y] * w[2] * (1 + 3 * v[x, y] + 4.5 * v[x, y] * v[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 3] = rho[x, y] * w[3] * (1 - 3 * u[x, y] + 4.5 * u[x, y] * u[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 4] = rho[x, y] * w[4] * (1 - 3 * v[x, y] + 4.5 * v[x, y] * v[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 5] = rho[x, y] * w[5] * (1 + 3 * (u[x, y] + v[x, y]) + 4.5 * (u[x, y] + v[x, y]) * (u[x, y] + v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 6] = rho[x, y] * w[6] * (1 + 3 * (v[x, y] - u[x, y]) + 4.5 * (v[x, y] - u[x, y]) * (v[x, y] - u[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 7] = rho[x, y] * w[7] * (1 + 3 * (-u[x, y] - v[x, y]) + 4.5 * (u[x, y] + v[x, y]) * (u[x, y] + v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 8] = rho[x, y] * w[8] * (1 + 3 * (u[x, y] - v[x, y]) + 4.5 * (u[x, y] - v[x, y])*(u[x, y] - v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            
            for k in range(0, 9):
                f[x, y, k] = f[x, y, k] * (1 - omega) + omega * feq[x, y, k] #colisão
     
        
    for x in range(0,n):
        for y in range(0,m):
            rho[x, y] = sum(f[x, y, : ]) 
            rho[0, y] = (f[0, y, 0] + f[0, y, 2] + f[0, y, 4] + 2 * (f[0, y, 3] * f[0, y, 6] + f[0, y, 7])) / (1.0 - u_lattice)
            
            for k in range(0, 9):
                tmpx[k] = f[x,y,k] * cx[k]
                tmpy[k] = f[x,y,k] * cy[k]
                
            u[x,y] = (1 / rho[x,y]) * sum(tmpx[:])
            v[x,y] = (1 / rho[x,y]) * sum(tmpy[:])
            
            
        for y in range(1, m-1):
            v[n-1, y] = 0.0 #we assume that, at the outlet, the y-component of the velocity should be zero
            u[0, y] = u_lattice
            
    for y in range(m-1, 0, -1):
    	for x in range(0, n-1):
    		f_linha[x, y, 2] = f[x, y-1, 2] 
    		f_linha[x, y, 6] = f[x+1, y-1, 6] 
    
    for y in range(m-1, 0, -1):
    	for x in range(n-1, 0, -1):
    		f_linha[x, y, 1] = f[x-1, y, 1]
    		f_linha[x, y, 5] = f[x-1, y-1, 5]
    
    for y in range(0, m-1):
    	for x in range(n-1, 0, -1):
    		f_linha[x, y, 4] = f[x, y+1, 4]
    		f_linha[x, y, 8] = f[x-1, y+1, 8]
     
    for y in range(0, m-1):
    	for x in range(0, n-1):
    		f_linha[x, y, 3] = f[x+1, y, 3]
    		f_linha[x, y, 7] = f[x+1, y+1, 7]
    
            
    for y in range(0, m):
        #known velocity on west boundary, west boundary = inlet 
        rhow = (f[0, y, 0] + f[0, y, 2] + f[0, y, 4] + 2 * (f[0, y, 3] * f[0, y, 6] + f[0, y, 7]))/(1.0 - u_lattice)
        f_linha[0, y, 1] = f[0, y, 3] + 2.0 * rhow * u_lattice / 3.0
        f_linha[0, y, 5] = f[0, y, 7] + rhow * u_lattice / 6.0
        f_linha[0, y, 8] = f[0, y, 6] + rhow * u_lattice / 6.0
        
    for x in range(0, n): 
        #bounce back on south boundary
        f_linha[x, 0, 2] = f[x, 0, 4]
        f_linha[x, 0, 5] = f[x, 0, 7]
        f_linha[x, 0, 6] = f[x, 0, 8]
        
    for x in range(0, n):
        #bounce back on north boundary
        f_linha[x, m-1, 4] = f[x, m-1, 2]
        f_linha[x, m-1, 8] = f[x, m-1, 6]
        f_linha[x, m-1, 7] = f[x, m-1, 5]
        
    for y in range(1, m):
        #open boundary condition at the outlet
        f_linha[n-1, y, 1] = 2.0 * f[n-2, y, 1] - f[n-3, y, 1]
        f_linha[n-1, y, 5] = 2.0 * f[n-2, y, 5] - f[n-3, y, 5]
        f_linha[n-1, y, 8] = 2.0 * f[n-2, y, 8] - f[n-3, y, 8]
    
    tmp = f
    f = f_linha
    f_linha = tmp 

# U = u/u_in

plotar = u/u_lattice
   

plt.plot(abs(plotar[500, :]))
plt.plot(abs(plotar[900, :]))
plt.show()