#include <stdio.h>
#include <stdlib.h>

int main(void) {
  float T[100], feq[100], f[100][2], f_linha[100][2], tmp[100][2];
  float omega, soma;
  int t, x, k;

  omega = 4.0/3.0;

  FILE *temperatura;

  temperatura = fopen("temperatura.txt", "r+");

  for(x=0;x<100;x++){
    T[x] = 0.0;
    feq[x] = 0.0;
    for(k=0;k<2;k++){
      f[x][k] = 0.0;
      f_linha[x][k] = 0.0;
      tmp[x][k] = 0.0;
    }
  }
  
  T[0] = 1.0;

  for(t=0;t<=200;t++){
    for(x=0;x<100;x++){
      soma = 0.0;
      for(k=0;k<2;k++){
        soma += f[x][k];
      }
      T[x] = soma;
      feq[x] = T[x] * 0.5;
      k = 0;
      for(k=0;k<2;k++){
        f[x][k] = f[x][k] * (1.0 - omega) + omega * feq[x]; //colisao
      }
      printf("%f\n", T[x]);
    }

    x=1; 
    for(x=1; x<99; x++){
      f_linha[x][0] = f[x+1][0];
      f_linha[x][1] = f[x-1][1];
    }

    f_linha[0][0] = f[1][0];
    f_linha[99][1] = f[98][1];

    f_linha[0][1] = 1.0 - f_linha[0][0];

    x = 0;
    k = 0;
    for(x=0;x<100;x++){
      for(k=0;k<2;k++){
        tmp[x][k] = f[x][k];
        f[x][k] = f_linha[x][k];
        f_linha[x][k] = tmp[x][k];
      }
    }
  }

  x = 0;
  for(x=0;x<100;x++){
    fprintf(temperatura, "%f\n", T[x]);
  }
  return 0;
}


}



